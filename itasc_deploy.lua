#!/usr/bin/env rttlua
-- -*- rttlua -*-

--This program checks a given model with the itasc meta-model and transforms it in to executable code, that will be executed

local umf=require("umf")
local utils=require("utils")
local itasc=require("itasc")
local rttlib=require("rttlib")
local rttros = require("rttros")
local itasc_orocos

Application       = itasc.Application
SetpointGenerator = itasc.SetpointGenerator
Sensor            = itasc.Sensor
iTaSC             = itasc.iTaSC
Robot             = itasc.Robot
Object            = itasc.Object
Rewire            = itasc.Rewire
SceneElement      = itasc.SceneElement
ExternalInput     = itasc.ExternalInput
Task              = itasc.Task
SkillTask         = itasc.SkillTask
TaskSetting       = itasc.TaskSetting
Solver            = itasc.Solver
Frame             = itasc.Frame
FSM               = itasc.FSM
VKC               = itasc.VKC
CC                = itasc.CC
Driver            = itasc.Driver
Rotation          = itasc.Rotation
Vector            = itasc.Vector

local ReportProp
local instance_of = umf.instance_of
NumberSpec        = umf.NumberSpec
StringSpec        = umf.StringSpec
BoolSpec          = umf.BoolSpec
FunctionSpec      = umf.FunctionSpec
EnumSpec          = umf.EnumSpec
TableSpec         = umf.TableSpec
ClassSpec         = umf.ClassSpec
ObjectSpec        = umf.ObjectSpec

function help()
   print( [=[
itasc_deply v0.1
validate, transform to executable code and execute itasc models.
  Usage:
     itasc_deploy OPTIONS <modelfile>
  <modelfile> should be written in file spec syntax, eg. file://my_ros_pkg#/folder/model.lua
  Options:
    -c    check only, return after model validation (wrt itasc metamodel)
    -d    framework to deploy the model with, currently supported: orocos (default)
    -q    quiet, no output
    -h    print help
    -l    (rtt) log level: Debug, Info, Warning, Error
    -r    report data using specified property file (file spec syntax)
         ]=] )
end

function check_file(f, name)
   if not utils.file_exists(f) then
      print("error: could not find " .. name)
      os.exit(1)
   end
   return f
end

function filespec2file(filespec)
    local _,_,filename=string.find(filespec, "file://(.*)")

    if filename then
      -- if package
      local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
      if ret~=nil then
         local packpath, res
         res, packpath = pcall(rttros.find_rospack, pack)
         if(string.find(file,"/(.*)")) then
           filename = packpath..file
         else
           filename = packpath.."/"..file
         end
      end
      if not utils.file_exists(filename) then
        print("non-existing file "..filename)
        os.exit(1)
      end 
    else
      print("Syntax error: Given file name '"..filespec.."' does not follow filespec syntax: file://ROSpackage#/path/to/file.extention")
      os.exit(1)
    end
    return filename
end

local argtab=utils.proc_args(arg)

if #arg<1 or argtab['-h'] then help(); os.exit(1); end

local loglevel_table = argtab['-l']
if loglevel_table then rtt.setLogLevel(loglevel_table[1]) end

local report_table = argtab['-r']
if report_table then 
    ReportProp=report_table[1] 
    local filename = filespec2file(ReportProp)
    print(check_file(filename,filename))
end

modelfile = filespec2file(arg[#arg])
print(check_file(modelfile))
model = dofile(modelfile)

if not model then 
    print("err: no model could be loaded from file " .. modelfile) 
    os.exit(1)
end

-------------------
-- conformity check
-------------------
if umf.check(model, itasc.Application_spec, (not argtab['-q']))~=0 then
   os.exit(1)
   print("Model does not conform to meta-model")
end

if argtab['-c'] then os.exit(0); end

--------------------------------------------------
-- transformation to executable code and execution
--------------------------------------------------
if (not argtab['-d']) or (argtab['-d'][1] == 'orocos') then
  itasc_orocos=require("itasc_orocos")
  itasc_orocos.deploy(model, ReportProp)
else
  print("Error: Unsupported framework " .. argtab['-d'][1] .. " to deploy the model") 
end
