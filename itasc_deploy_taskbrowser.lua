#!/usr/bin/env rttlua
-- -*- rttlua -*-

--This program checks a given model with the itasc meta-model and transforms it in to executable code, that will be executed
-- this taskbrowser version will start in an Orocos taskbrowser and will use following settings:

-- are the global parameters set?
if not model2deploy then
   print("error: global parameter 'model2deploy' not specified")
   os.exit(1)
end
local DoOnlyModelCheck = true
if not onlyModelCheck then
  DoOnlyModelCheck = false
end
local FileName = model2deploy
local DoOrocosExec = true
local ReportProp = false -- fill in with a filespec pointing to the reporter configuration file to use the reporter
if reporterCPF then
  ReportProp = reporterCPF
end
---
local umf=require("umf")
local utils=require("utils")
local itasc=require("itasc")
local rttlib=require("rttlib")
local rttros = require("rttros")
local itasc_orocos

Application       = itasc.Application
SetpointGenerator = itasc.SetpointGenerator
Sensor            = itasc.Sensor
iTaSC             = itasc.iTaSC
Robot             = itasc.Robot
Object            = itasc.Object
Rewire            = itasc.Rewire
SceneElement      = itasc.SceneElement
ExternalInput     = itasc.ExternalInput
Task              = itasc.Task
SkillTask         = itasc.SkillTask
TaskSetting       = itasc.TaskSetting
Solver            = itasc.Solver
Frame             = itasc.Frame
FSM               = itasc.FSM
VKC               = itasc.VKC
CC                = itasc.CC
Driver            = itasc.Driver
Rotation          = itasc.Rotation
Vector            = itasc.Vector

local instance_of = umf.instance_of
NumberSpec        = umf.NumberSpec
StringSpec        = umf.StringSpec
BoolSpec          = umf.BoolSpec
FunctionSpec      = umf.FunctionSpec
EnumSpec          = umf.EnumSpec
TableSpec         = umf.TableSpec
ClassSpec         = umf.ClassSpec
ObjectSpec        = umf.ObjectSpec

function check_file(f, name)
   if not utils.file_exists(f) then
      print("error: could not find " .. name)
      os.exit(1)
   end
   return f
end

function filespec2file(filespec)
    local _,_,filename=string.find(filespec, "file://(.*)")
    
    if filename then
      -- if package
      local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
      if ret~=nil then
         local packpath, res
         res, packpath = pcall(rttros.find_rospack, pack)
         if(string.find(file,"/(.*)")) then
           filename = packpath..file
         else
           filename = packpath.."/"..file
         end
      end
      if not utils.file_exists(filename) then
        print("non-existing file "..filename)
        os.exit(1)
      end 
    else
      print("Syntax error: Given file name '"..filespec.."' does not follow filespec syntax: file://ROSpackage#/path/to/file.extention")
      os.exit(1)
    end
    return filename
end

if ReportProp then 
    local filename = filespec2file(ReportProp)
    print("Reporter configuration to be loaded from: \n"..check_file(filename,filename))
end

modelfile = filespec2file(FileName)
print("iTaSC model to be loaded from: \n"..check_file(modelfile))
model = dofile(modelfile)

if not model then 
    print("err: no model could be loaded from file " .. modelfile) 
    os.exit(1)
end

-------------------
-- conformity check
-------------------
if umf.check(model, itasc.Application_spec, true)~=0 then
   os.exit(1)
   print("Model does not conform to meta-model")
end

if DoOnlyModelCheck then os.exit(0); end

--------------------------------------------------
-- transformation to executable code and execution
--------------------------------------------------
if DoOrocosExec then
  itasc_orocos=require("itasc_orocos")
  itasc_orocos.deploy(model, ReportProp)
else
  print("Error: Unsupported framework to deploy the model") 
end
