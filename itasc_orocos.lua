--
-- This file is part of iTaSC DSL.
--
-- (C) 2012, 2013 Dominick Vanthienen, dominick.vanthienen@mech.kuleuven.be,
-- Department of Mechanical Engineering, Katholieke Universiteit
-- Leuven, Belgium.
--
-- You may redistribute this software and/or modify it under either
-- the terms of the GNU Lesser General Public License version 2.1
-- (LGPLv2.1 <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>)
-- or (at your discretion) of the Modified BSD License: Redistribution
-- and use in source and binary forms, with or without modification,
-- are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
-- notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
-- copyright notice, this list of conditions and the following
-- disclaimer in the documentation and/or other materials provided
-- with the distribution.
-- 3. The name of the author may not be used to endorse or promote
-- products derived from this software without specific prior
-- written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
-- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
-- WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
-- NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--

-- functionallity to deploy an itasc model with an orocos_toolchain based implementation
local rttlib = require("rttlib") 
local rttros = require("rttros")
local strict = require("strict")
local umf    = require("umf")
local itasc  = require("itasc")

Application       = itasc.Application
SetpointGenerator = itasc.SetpointGenerator
Sensor            = itasc.Sensor
iTaSC             = itasc.iTaSC
Robot             = itasc.Robot
Object            = itasc.Object
Rewire            = itasc.Rewire
SceneElement      = itasc.SceneElement
ExternalInput     = itasc.ExternalInput
Task              = itasc.Task
SkillTask         = itasc.SkillTask
Solver            = itasc.Solver
Frame             = itasc.Frame
FSM               = itasc.FSM
VKC               = itasc.VKC
CC                = itasc.CC
Driver            = itasc.Driver
Rotation          = itasc.Rotation
Vector            = itasc.Vector
TaskSetting       = itasc.TaskSetting
local is_Rewire            = itasc.is_Rewire
local is_SceneElement      = itasc.is_SceneElement
local is_SceneElement_Task = itasc.is_SceneElement_Task
local is_Robot             = itasc.is_Robot
local is_table             = itasc.is_table
local is_table_notFrame    = itasc.is_table_notFrame
local is_VKC_Task          = itasc.is_VKC_Task
local is_package           = itasc.is_package
local is_component         = itasc.is_component
local is_Task              = itasc.is_Task
local is_Robot_Object      = itasc.is_Robot_Object
local is_FSM               = itasc.is_FSM
local is_Rotation          = itasc.is_Rotation
local is_CC                = itasc.is_CC
local is_VKC               = itasc.is_VKC
local is_Solver            = itasc.is_Solver
local is_component_or_FSM  = itasc.is_component_or_FSM
local is_tasklevel_FSM     = itasc.is_tasklevel_FSM
local is_itasclevel_FSM    = itasc.is_itasclevel_FSM
local has_config           = itasc.has_config
local has_driver           = itasc.has_driver
local is_meta              = itasc.is_meta
local is_ExternalInput     = itasc.is_ExternalInput
local is_SetpointGenerator          = itasc.is_SetpointGenerator
local is_Driver                     = itasc.is_Driver
local is_applicationlevel_component = itasc.is_applicationlevel_component
local is_itascORtasklevel_component = itasc.is_itascORtasklevel_component
local is_tasklevel_component        = itasc.is_tasklevel_component
local is_itasclevel_component       = itasc.is_itasclevel_component
local is_applicationlevel_FSM       = itasc.is_applicationlevel_FSM
local has_compositelevel_supervisor = itasc.has_compositelevel_supervisor
local is_applicationlevel_component_or_supervisor = itasc.is_applicationlevel_component_or_supervisor
local instance_of = umf.instance_of
-- maybe better to put these values in application.cpf!!!
local application_period = 0.01
local priority = 98
local schedtype = rtt.globals.ORO_SCHED_RT
local event_firer_file = rttros.find_rospack("itasc_core").."/scripts/reduced_event_firer.lua" --always this file, should not change!
local BUFFERED_EVENT_CONN_SIZE = 64 --size of the buffer for buffered event port connections
local BUFFERED_LOCKED_EVENT_CONN_SIZE = 32 --size of the buffer for locked buffered event port connections
local EVENT_FIRER_PERIOD = 0.01
local REPORTER_PERIOD = 0.01

module("itasc_orocos", package.seeall)

-- this info should go into a knowledgebase or be derived from the property file in question?! The metamodel check could use this too!
function load_prop_spec_list()
    local prop_spec_list = {}
    prop_spec_list.chain = {propName = 'chainDef', modelType = 'table', orocosType = rtt.Variable("string[]")}
    prop_spec_list.constraint_value = {portName = 'InputGoal', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    prop_spec_list.execution_time = {portName = 'InputTime', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    prop_spec_list.maximum_velocity = {propName = 'max_vel', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    prop_spec_list.maximum_acceleration = {propName = 'max_acc', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    prop_spec_list.weight = {propName = 'W', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    prop_spec_list.proportional_gain = {propName = 'Kp', modelType = 'table', orocosType = rtt.Variable("float64[]")}
    return prop_spec_list
end

--- load conversion table
-- TODO move this to metamodel/knowledge base?
function load_conversion_table()
    local conversion_table={}
    conversion_table["string[]"] = {}
    conversion_table["string[]"].type = 'table'
    conversion_table["string[]"].convert = function (input)
      local output = rtt.Variable("string[]")
      local input_tab = {}
      output:resize(#input)
      -- ignore meta data
      for i,v in ipairs(input) do
        input_tab[i]=v
      end
      output:fromtab(input_tab)        
      return output
    end
    conversion_table["strings"] = {}
    conversion_table["strings"].type = 'table'
    conversion_table["strings"].convert = function (input)
      local output = rtt.Variable("strings")
      local input_tab = {}
      output:resize(#input)
      -- ignore meta data
      for i,v in ipairs(input) do
        input_tab[i]=v
      end
      output:fromtab(input_tab)
      return output
    end
    conversion_table["float64[]"] = {}
    conversion_table["float64[]"].type = 'table'
    conversion_table["float64[]"].convert = function (input)
      local output = rtt.Variable("float64[]")
      local input_tab = {}
      output:resize(#input)
      -- ignore meta data
      for i,v in ipairs(input) do
        input_tab[i]=v
      end
      output:fromtab(input_tab)
      return output
    end
    conversion_table["array"] = {}
    conversion_table["array"].type = 'table'
    conversion_table["array"].convert = function (input)
      local output = rtt.Variable("array")
      local input_tab = {}
      output:resize(#input)
      -- ignore meta data
      for i,v in ipairs(input) do
        input_tab[i]=v
      end
      output:fromtab(input_tab)
      return output
    end
    conversion_table["double"] = {}
    conversion_table["double"].type = 'double'
    conversion_table["double"].convert = function (input)
      local output = rtt.Variable("double")
      output = input
      return output
    end
    return conversion_table
end

--- convert type
-- @param input object to convert
-- @param outputType
-- TODO move this to metamodel/knowledge base?
function convert_type(input, outputType)
    local conv_tab = load_conversion_table()
    for i,v in pairs(conv_tab) do
      if i==outputType:getType() and v.type==type(input) then
        return conv_tab[i].convert(input)
      end
    end
    rtt.logl("Error", "No conversion from "..type(input).." to "..outputType:getType().." found.")
    os.exit(1)
end

function filespec2file(filespec)
    local _,_,filename=string.find(filespec, "file://(.*)")

    -- if package
    local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
    if ret~=nil then
       local packpath
       res, packpath = pcall(rttros.find_rospack, pack)
       if(string.find(file,"/(.*)")) then
         filename = packpath..file
       else
         filename = packpath.."/"..file
       end
    end
    return filename
end

--- import an orocos package
-- @param pkg package to import
-- @param depl deployer in which to load the package
function import_package(pkg, depl)
    --TODO delete this loglevel hack once the unnecessary ERROR message that is thrown is deleted from rtt/ocl
    local loglevel_saved = rtt.getLogLevel()
    rtt.setLogLevel("Never")
    local res = depl:import(pkg.package)
    rtt.setLogLevel(loglevel_saved)
end

--- import metadata from a rospackage
-- @param obj object with rospackage entry
-- @param depl deployer
function import_metadata(obj, depl)
      res, packpath = pcall(rttros.find_rospack, obj.package)
      if res and loadfile(packpath.."/metadata.lua") then
        obj._metadataFile = loadfile(packpath.."/metadata.lua")()
      end
end

---load an orocos component
-- @param obj object to load as a component with name of obj or obj class name+_+obj parent name (if obj.name=nil)
-- @param depl deployer in which to load component
function load_component(obj, depl)
    if obj.name and obj.type then
      depl:loadComponent(obj.name, obj.type)
      return obj.name
    else 
      depl:loadComponent(obj:class().name .. "_" .. obj._parent.name, obj.type)
      return obj:class().name .. "_" .. obj._parent.name
    end
end

---create an empty rtt lua component
-- @param fsm fsm object that will be loaded in supervisor
-- @param depl deployer in which to load component
function create_lua_component(fsm, depl)
    depl:loadComponent(fsm._parent.name.."_supervisor", "OCL::LuaComponent")
    return fsm._parent.name.."_supervisor"
end

---set activity
-- @param obj object linked with a component
-- @param depl deployer
-- @param period a double
-- @param priority a double
-- @param schedtype rtt.globals.ORO_SCHED_RT or rtt.globals.ORO_SCHED_OTHER 
function set_activity(obj, depl, period, priority, schedtype)
    if obj._component_name then
      depl:setActivity(obj._component_name, period, priority, schedtype)
    else 
      rtt.logl("Error","No component assigned to " .. obj.name .. ", unable to set activity")
      return false
    end
end

--- add rtt peers
-- @param peer string with the name of the peer
-- @param depl deployer
-- @param component_name component_name string that will get peer as a peer
function add_peers(peer, depl, component_name)
    local component = depl:getPeer(component_name)
    if component~=peer then
      rtt.logl("Info","add peer "..peer._component_name.." to "..component_name)
      component:addPeer(peer._component)
    end
end

--- connect rtt peers
-- @param peer string with the name of the peer 
-- @param depl deployer
-- @param component_name component_name string that will get peer as a peer
function connect_peers(peer, depl, component_name)
    local component = depl:getPeer(component_name)
    if component~=peer then
      rtt.logl("Info","add peer "..peer._component_name.." to "..component_name)
      component:addPeer(peer._component)
      rtt.logl("Info","add peer "..component_name.." to "..peer._component_name)
      peer._component:addPeer(component)
    end
end

--- load a skill in a supervisor
-- @param fsm fsm object
function load_skill(fsm)
    local filename = filespec2file(fsm.fsm)
    --rtt.logl("Debug","supervisor= "..filename)
    fsm._component:exec_file(filename)
end

--- configure a skill
-- @param fsm fsm object
function configure_skill(fsm)
    fsm._component:configure()
end

--- load marshalling service
-- @param obj object with ref to component that needs the marshalling service
function load_marshalling(obj, depl)
    depl:loadService(obj._component_name, "marshalling")
end

--- find and update properties
-- @param obj object of which component to load properties into
-- @param tab table with properties to fill in
function find_update_properties(obj, tab)
    local prop_specs = load_prop_spec_list() 
    local prop_exists, port_exists = false, false
    for i, v in pairs(tab) do
      if prop_specs[i] then
        -- TODO this check should move to the metamodel conformity check
        if(type(v)==prop_specs[i].modelType) then
          if(prop_specs[i].propName) then
            prop_exists = false
            local prop_tab = obj._component:getPropertyNames()
            for ii, vprop in pairs(prop_tab) do
              if vprop == prop_specs[i].propName then
                prop_exists = true
                break
              end
            end
            if not prop_exists then
              rtt.logl("Error","The component "..obj._component_name.." doesn't have a property "..prop_specs[i].propName.." required to fill in "..tostring(i)); os.exit(1)
            end
            local prop = obj._component:getProperty(prop_specs[i].propName)
            prop:set(convert_type(v, prop_specs[i].orocosType))
          elseif(prop_specs[i].portName) then
            port_exists = false
            local port_tab = obj._component:getPortNames()
            for ii, vport in pairs(port_tab) do
              if vport == prop_specs[i].portName then
                port_exists = true
                break
              end
            end
            if not port_exists then
              rtt.logl("Error","The component "..obj._component_name.." doesn't have a port "..prop_specs[i].portName.." required to fill in "..prop_specs[i].portName); os.exit(1)
            end
            local port = rttlib.port_clone_conn(obj._component:getPort(prop_specs[i].portName))
            port:write(convert_type(v, prop_specs[i].orocosType))
            rtt.logl("Info","Successfully wrote "..prop_specs[i].portName.." of component "..obj._component_name)
          else
            rtt.logl("Error", "Corrupted prop_spec_list in itasc_orocos.lua: not a prop nor port"); os.exit(1)
          end
        else
          rtt.logl("Error", "The property "..i.." is of type "..type(v)..", expected "..prop_specs[i].modelType); os.exit(1)
        end
      end
    end 
end

--- load property file
-- @param obj object that contains a component and configuration data
function update_properties(obj)
    local updateProps = obj._component:provides("marshalling"):getOperation("updateProperties")
    -- load first property files
    for i, v in ipairs(obj.config) do
      if type(v)=='string' then
        local filename = filespec2file(v)
        updateProps(filename)
      end
    end
    -- load then specific properties
    for i, v in ipairs(obj.config) do
      if type(v)=='table' then
        find_update_properties(obj, v)
      end
    end
end

--- Create an inverse port of a given port without connecting
-- The default name will be the same as the given port.
-- @param p port to create inverse port.
-- @param suffix string to append name of new port (optional)
-- @param cname alternative name for port (optional).
function port_clone_unconn(p, suffix, cname)
   local inf = p:info()
   local suf = suffix or ""
   local cname = cname or inf.name

   local cl
   if inf.porttype == 'in' then
      cl = rtt.OutputPort.new(inf.type, cname .. suf, "Inverse port of " .. inf.name)   
   elseif inf.porttype == 'out' then
      cl = rtt.InputPort.new(inf.type, cname .. suf, "Inverse port of " .. inf.name)
   else   
      error("unkown port type: " .. utils.tab2str(inf))
   end 
   return cl
end

--- connect timer
-- @param obj object that links to component to connect timer with
-- @param depl deployer
-- @param timer name of the timer component
function connect_timer(obj, depl, timer)
    local cp = rtt.Variable('ConnPolicy')
    local portA = obj._component_name..".trigger"
    local portB = timer..".timeout"
    depl:connect(portA, portB, cp)
end

--- connect_itasclevel_supervisor
-- @param itasc_obj object with link to itasc level supervisor
-- @param depl deployer
-- @param app_obj object with link to application level supervisor
function connect_itasclevel_supervisor(itasc_obj, depl, app_obj)
    local cp = rtt.Variable('ConnPolicy')
    local bcp = rtt.Variable('ConnPolicy')
    bcp.type=1   -- type buffered
    bcp.size=BUFFERED_EVENT_CONN_SIZE  -- buffer size

    depl:connect(itasc_obj._component_name..".itasc_priority_events_in", 
                 app_obj._component_name..".application_priority_events_out", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_priority_events_in with "..app_obj._component_name..".application_priority_events_out")
    depl:connect(itasc_obj._component_name..".itasc_priority_events_out",
                 app_obj._component_name..".application_priority_events_in", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_priority_events_out with "..app_obj._component_name..".application_priority_events_in")
    depl:connect(itasc_obj._component_name..".itasc_common_events_in",
                 app_obj._component_name..".application_common_events_out", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_common_events_in with "..app_obj._component_name..".application_common_events_out")
    depl:connect(itasc_obj._component_name..".itasc_common_events_out",
                 app_obj._component_name..".application_common_events_in", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_common_events_out with "..app_obj._component_name..".application_common_events_in")
    -- connection of trigger events shouldn't be needed but for consistency it is done here
    depl:connect(itasc_obj._component_name..".itasc_trigger_events_in",
                 app_obj._component_name..".application_trigger_events_out", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_trigger_events_in with "..app_obj._component_name..".application_trigger_events_out")
    depl:connect(itasc_obj._component_name..".itasc_trigger_events_out",
                 app_obj._component_name..".application_trigger_events_in", bcp)
    rtt.logl("Info","Connecting "..itasc_obj._component_name..".itasc_trigger_events_out with "..app_obj._component_name..".application_trigger_events_in")
end

--- connect_tasklevel_supervisor
-- @param task_obj object with link to task level supervisor
-- @param depl deployer
-- @param itasc_obj object with link to itasc level supervisor
-- @param app_obj object with link to application level supervisor
function connect_tasklevel_supervisor(task_obj, depl, itasc_obj, app_obj)
    local cp = rtt.Variable('ConnPolicy')
    local bcp = rtt.Variable('ConnPolicy')
    bcp.type=1   -- type buffered
    bcp.size=BUFFERED_EVENT_CONN_SIZE  -- buffer size
    local lcp = rtt.Variable('ConnPolicy')
    lcp.type=1        -- type buffered
    lcp.size=BUFFERED_LOCKED_EVENT_CONN_SIZE  -- buffer size
    lcp.lock_policy=1 -- locked TODO: needed?
    local ret=true

    -- connections with application level supervisor
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_priority_events_in", 
                 app_obj._component_name..".application_priority_events_out", bcp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_priority_events_out",
                 app_obj._component_name..".application_priority_events_in", bcp)
    if not ret then
      rtt.logl("Error","Unable to connect "..task_obj._component_name.." with "..app_obj._component_name)
    else
      rtt.logl("Debug","Succesfully connected "..task_obj._component_name.." with "..app_obj._component_name)
    end
    ret = true
    -- connections with itasc level supervisor
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_priority_events_in", 
                 itasc_obj._component_name..".itasc_priority_events_out", cp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_priority_events_out",
                 itasc_obj._component_name..".itasc_priority_events_in", cp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_common_events_in", 
                 itasc_obj._component_name..".itasc_common_events_out", bcp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_common_events_out",
                 itasc_obj._component_name..".itasc_common_events_in", bcp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_trigger_events_in", 
                 itasc_obj._component_name..".itasc_trigger_events_out", cp)
    ret = depl:connect(task_obj._component_name.."."..task_obj._parent.name.."_trigger_events_out",
                 itasc_obj._component_name..".itasc_trigger_events_in", lcp)
    if not ret then
      rtt.logl("Error","Unable to connect "..task_obj._component_name.." with "..itasc_obj._component_name)
    else
      rtt.logl("Debug","Succesfully connected "..task_obj._component_name.." with "..itasc_obj._component_name)
    end

end

--- event connect
-- @param obj object with a reference to a component with possibly an event port
-- @param depl deployer
-- @param sup object with supervisor to connect to
function event_connect(obj, depl, sup)
    local portNames
    local bcp = rtt.Variable('ConnPolicy')
    bcp.type=1   -- type buffered
    bcp.size=BUFFERED_EVENT_CONN_SIZE  -- buffer size
    -- define supervisor type
    local supType = sup._parent:class().name
    if supType == 'Task' then supType = sup._parent.name
    elseif supType == 'itasc' then supType = 'itasc'
    elseif supType == 'application' then supType = 'application'
    else rtt.logl("Error", "Supervisor "..sup._component_name.." is of an unknown level class "..supType); os.exit(1)
    end
    -- single connection function
    function type_event_connect(event_type, e_dir, obj_comp_name)
      local e_dir_inv
      if e_dir=="in" then e_dir_inv="out" else e_dir_inv="in" end
      depl:connect(obj_comp_name.."."..event_type.."_events_"..e_dir, sup._component_name.."."..supType.."_"..event_type.."_events_"..e_dir_inv, bcp)
    rtt.logl("Info","connecting "..obj_comp_name.."."..event_type.."_events_"..e_dir .." with "..sup._component_name.."."..supType.."_"..event_type.."_events_"..e_dir_inv)
    end
    -- general connection function
    function general_event_connect(portNames, obj_comp_name)
      for i,v in ipairs(portNames) do    
        -- default events
        if v=="events_out" then
          depl:connect(obj_comp_name..".events_out", sup._component_name.."."..supType.."_common_events_in", bcp)
        rtt.logl("Info","connecting "..obj_comp_name..".events_out" .." with "..sup._component_name.."."..supType.."_common_events_in")
        elseif v=="events_in" then
          depl:connect(obj_comp_name..".events_in", sup._component_name.."."..supType.."_common_events_out", bcp)
        rtt.logl("Info","connecting "..obj_comp_name..".events_in" .." with "..sup._component_name.."."..supType.."_common_events_out")
        -- typed events
        elseif v=="common_events_out" then type_event_connect("common","out", obj_comp_name)
        elseif v=="common_events_in" then type_event_connect("common","in", obj_comp_name)
        elseif v=="priority_events_out" then type_event_connect("priority","out", obj_comp_name)
        elseif v=="priority_events_in" then type_event_connect("priority","in", obj_comp_name)
        elseif v=="trigger_events_out" then type_event_connect("trigger","out", obj_comp_name)
        elseif v=="trigger_events_in" then type_event_connect("trigger","in", obj_comp_name) 
        end
      end
    end
    -- find component
    if obj._component then
      portNames = obj._component:getPortNames()
      obj_comp_name = obj._component_name
      general_event_connect(portNames, obj_comp_name)
    end
    if obj._supervisor then
      portNames = obj._supervisor:getPortNames()
      obj_comp_name = obj._supervisor_name
      general_event_connect(portNames, obj_comp_name)
    end
end

--- connect referenced eventports
-- @param obj Rewire object with a reference to a component with possibly an event port
-- @param depl deployer
-- @param sup object with supervisor to connect to
function connect_referenced_eventports(obj, depl, sup)
    if obj._tgtref then
      event_connect(obj._tgtref, depl, sup)
    end
    if obj._srcref then
      event_connect(obj._srcref, depl, sup)
    end
end

--- connect cc with vkc
-- @param task Task object
-- @param depl deployer
-- for now only if cartesian_motion, others automatically by Scene
function connect_cc_vkc(task, depl)
    if task.vkc and task.vkc.package=='cartesian_motion' then
      if task.cc.package=='cartesian_motion' then
        local cp = rtt.Variable('ConnPolicy')
        depl:connect(task.cc._component_name..".ChifT", task.vkc._component_name..".T_o1_o2_out", cp)
      else
        rtt.logl("Error","Unable to connect "..task.vkc.package.." with "..task.vkc.package)
      end
    end
end

--- connect
-- @param src source object
-- @param tgt target object
-- @param rewireTab table of rewire statements
-- @param depl deployer
function connect(src, tgt, rewireTab, depl)
    local cp = rtt.Variable('ConnPolicy')
    local roscp = rtt.Variable('ConnPolicy')
    roscp.transport = 3
    local srcType
    local srcProps
    local tgtType
    local tgtProps
    -- are src and tgt Orocos components or ROS nodes?
    -- -- reduce combination possibilities: 
    -- -- ROS - ORO or ORO - ROS => ROS - ORO
    if src._component and not tgt._component then
      local temp = src
      src = tgt
      tgt = temp
      for i,tab in ipairs(rewireTab) do
        temp = tab.from
        tab.from = tab.to
        tab.to = temp
      end
    end
    if src._component and src._metadataFile then
      rtt.logl("Warning","Not supported combination of component with extra metadata file for "..src._component_name..", will ignore metadata file for port/topic connections.")
    end
    local srcPortNames = {}
    local srcInPortNames = {}
    local srcOutPortNames = {}
    local srcMetadata
    local srcMetadataProp
    local srcMetadataFun
    local srcComp
    if src._component then
      srcType = 'ORO'
      srcComp = src._component
      srcProps = srcComp:getPropertyNames()
      rtt.logl("Debug","--- connection of src: "..src._component_name.."...")
    elseif src._metadataFile then
      srcMetadataF = src._metadataFile
      --TODO do this on a port by port basis!
      if srcMetadataF.ports then
        for a,b in pairs(srcMetadataF.ports) do
          if b.fw and b.fw == 'ros' then
            srcMetadataF.ros = true
          end
        end
      end
      if srcMetadataF.ros and srcMetadataF.orocos then
        rtt.logl("Error","UNDER CONSTRUCTION: metadata from a mixed framework is unsupported at the moment")
        -- then this procedure has to be repeated: once for orocos things, once for ros things
      elseif srcMetadataF.ros then 
        srcType = 'ROS'
      elseif srcMetadataF.orocos then
        srcType = 'ORO'
        -- ORO type without _component
        if #(srcMetadataF.orocos)>1 then
          rtt.logl("Error","UNDER CONSTRUCTION: connection of ports of multiple external components (composite component) unsupported at the moment")
        end
        local Pdepl = depl:getPeers()
        srcComp = false
        for i,j in pairs(Pdepl) do
          if src._setup and src._setup.orocos then
            if j==src._setup.orocos[srcMetadataF.orocos[1]] then
              srcComp = depl:getPeer(src._setup.orocos[srcMetadataF.orocos[1]])
            end
          elseif j==srcMetadataF.orocos[1] then
              srcComp = depl:getPeer(srcMetadataF.orocos[1])
          end
        end
        if not srcComp then
          rtt.logl("Error","Unable to find the component named "..srcMetadataF.orocos[1].." or "..src._setup.orocos[srcMetadataF.orocos[1]].." as mentioned in the corresponding meta data file")
        end
        srcProps = srcComp:getPropertyNames() 
      else
        rtt.logl("Error","Metadata connection info of unrecognized framework (not ROS or OROCOS")
      end
      rtt.logl("Debug","--- connection of src: "..src.package.."...")
    else
      rtt.logl("Error","No component or package specified for source")
    end

    if tgt._component and tgt._metadataFile then
      rtt.logl("Warning","Not supported combination of component with extra metadata file for "..tgt._component_name..", will ignore metadata file for port/topic connections.")
    end
    local tgtPortNames = {}
    local tgtInPortNames = {}
    local tgtOutPortNames = {}
    local tgtMetadata
    local tgtMetadataProp
    local tgtMetadataFun
    local tgtComp
    if tgt._component then
      tgtType = 'ORO'
      tgtComp = tgt._component
      tgtProps = tgtComp:getPropertyNames()
      rtt.logl("Debug","with tgt: "..tgt._component_name.." ---")
    elseif tgt._metadataFile then
      tgtMetadataF = tgt._metadataFile
      --TODO do this on a port by port basis!
      if tgtMetadataF.ports then
        for a,b in pairs(tgtMetadataF.ports) do
          if b.fw and b.fw == 'ros' then
            tgtMetadataF.ros = true
          end
        end
      end
      if tgtMetadataF.ros and tgtMetadataF.orocos then
        rtt.logl("Error","UNDER CONSTRUCTION: metadata from a mixed framework is unsupported at the moment")
        -- then this procedure has to be repeated: once for orocos things, once for ros things
      elseif tgtMetadataF.ros then 
        tgtType = 'ROS'
      elseif tgtMetadataF.orocos then
        tgtType = 'ORO'
        -- ORO type without _component
        if #(tgtMetadataF.orocos)>1 then
          rtt.logl("Error","UNDER CONSTRUCTION: connection of ports of multiple external components (composite component) unsupported at the moment")
        end
        local Pdepl = depl:getPeers()
        tgtComp = false
        for i,j in pairs(Pdepl) do
          if j==tgtMetadataF.orocos[1] then
            if tgt._setup and tgt._setup.orocos then
              tgtComp = depl:getPeer(tgt._setup.orocos[tgtMetadataF.orocos[1]])
            else
              tgtComp = depl:getPeer(tgtMetadataF.orocos[1])
            end
          end
        end
        if not tgtComp then
          rtt.logl("Error","Unable to find the component "..tgtMetadataF.orocos[1].." as mentioned in the corresponding meta data file")
        end
        tgtProps = tgtComp:getPropertyNames() 
      else
        rtt.logl("Error","Metadata connection info of unrecognized framework (not ROS or OROCOS")
      end
      rtt.logl("Debug","--- connection of tgt: "..tgt.package.."...")
    else
      rtt.logl("Error","No component or package specified for target")
    end

    --look for metadata in ORO type objects
    if srcType=='ORO' then
      --TODO use the rttlib.tc_has_property functionallity once upgraded to new ocl
      for i,v in ipairs(srcProps) do
        if v=="metadata" then 
          srcMetadataProp = srcComp:getProperty("metadata") 
          srcMetadataFun = loadstring("return "..srcMetadataProp:get())
          if srcMetadataFun then
            srcMetadata=srcMetadataFun()
          else
            rtt.logl("Error","Syntax error in the metadata property of component "..srcComp:getName())
          end
          break
        end
      end
      if not srcMetadata then
          rtt.logl("Error","no metadata found for src: "..srcComp:getName())
      end
    elseif srcType=='ROS' then
      srcMetadata=srcMetadataF
    end
    if tgtType=='ORO' then
      for i,v in ipairs(tgtProps) do
        if v=="metadata" then 
          tgtMetadataProp = tgtComp:getProperty("metadata") 
          tgtMetadataFun = loadstring("return "..tgtMetadataProp:get())
          if tgtMetadataFun then
            tgtMetadata=tgtMetadataFun()
          else
            rtt.logl("Error","Syntax error in the metadata property of component "..tgtComp:getName())
          end
          break
        end
      end
      if not tgtMetadata then
          rtt.logl("Error","no metadata found for tgt: "..tgtComp:getName())
      end
    elseif tgtType=='ROS' then
      tgtMetadata=tgtMetadataF
    end
    --for each table in rewireTab
    for i,tab in ipairs(rewireTab) do
    -- -- ALL - ALL
      if tab.from == "all" and tab.to == "all" then
        if srcType=='ORO' then
          srcPortNames = srcComp:getPortNames() 
          -- sort input from output ports for src
          for j,pname in ipairs(srcPortNames) do
            if srcComp:getPort(pname):info().porttype=='in' then
              srcInPortNames[#srcInPortNames+1] = pname
            else
              srcOutPortNames[#srcOutPortNames+1] = pname
            end
          end
        else
          for j,k in pairs(srcMetadata.ports) do
            if k.direction=='input' then
              srcInPortNames[#srcInPortNames+1] = j
            elseif k.direction=='output' then
              srcOutPortNames[#srcOutPortNames+1] = j
            else
              rtt.logl("Error","Syntax error in metadata file of package "..src.package..": "..j.." has direction "..k.direction.." (!=input OR output)")
            end
          end
        end
        -- >> ORO - ORO or ROS - ORO << --
        if tgtType=='ORO' then
          tgtPortNames = tgtComp:getPortNames() 
          -- sort input from output ports for tgt
          for j,pname in ipairs(tgtPortNames) do
            -- input ports of tgt
            if tgtComp:getPort(pname):info().porttype=='in' then
              tgtInPortNames[#tgtInPortNames+1] = pname
              local srcTgtConn = false
              for k, srcname in ipairs(srcOutPortNames) do
                -- look for output port of src with same type
                local tgtPortType = tgtComp:getPort(pname):info().type
                local srcPortType
                if srcType == 'ORO' then
                  srcPortType = srcComp:getPort(srcname):info().type
                else 
                  srcPortType = srcMetadata.ports[srcname].rtt_type
                end
                
                if tgtPortType==srcPortType then
                  -- compare meta data of both ports
                  if tgtMetadata and srcMetadata 
                  and tgtMetadata.ports[pname] and srcMetadata.ports[srcname]
                  and tgtMetadata.ports[pname].type==srcMetadata.ports[srcname].type 
                  and tgtMetadata.ports[pname].geom_sem and srcMetadata.ports[srcname].geom_sem 
                  and tgtMetadata.ports[pname].geom_sem==srcMetadata.ports[srcname].geom_sem
                  then
                    if srcTgtConn == true and srcType=='ORO' then 
                      rtt.logl("Error","Doubtfull connection possibilities between "..srcComp:getName().."."..pname.." and the ports of "..tgtComp:getName())
                    elseif srcTgtConn == true then
                      rtt.logl("Error","Doubtfull connection possibilities between "..src.package.."."..pname.." and the ports of "..tgtComp:getName())
                    elseif srcType=='ORO' then
                  rtt.logl("Info","Connecting "..srcComp:getName().."."..srcname.." and "..tgtComp:getName().."."..pname)
                  depl:connect(srcComp:getName().."."..srcname, tgtComp:getName().."."..pname, cp)
                  srcTgtConn = true
                    else
                  rtt.logl("Info","Connecting "..srcMetadata.ports[srcname].topic.."."..srcname.." and "..tgtComp:getName().."."..pname)
                  roscp.name_id = srcMetadata.ports[srcname].topic
                  depl:stream(tgtComp:getName().."."..pname, roscp)
                  srcTgtConn = true
                    end
                  end
                end
              end
              if not srcTgtConn and srcType=='ORO' then
                rtt.logl("Warning","Could not connect "..pname.." of "..tgtComp:getName().." with a port of component "..srcComp:getName()..": is this an error?")
              elseif not srcTgtConn then
                rtt.logl("Warning","Could not connect "..pname.." of "..tgtComp:getName().." with a topic of package "..src.package..": is this an error?")
              end
            -- output ports of tgt
            else
              tgtOutPortNames[#tgtOutPortNames+1] = pname
              local srcTgtConn = false
              for k, srcname in ipairs(srcInPortNames) do
                -- look for output port of src with same type
                local tgtPortType = tgtComp:getPort(pname):info().type
                local srcPortType
                if srcType == 'ORO' then
                  srcPortType = srcComp:getPort(srcname):info().type
                else 
                  srcPortType = srcMetadata.ports[srcname].rtt_type
                end

                if tgtPortType==srcPortType then
                  -- compare meta data of both ports
                  if tgtMetadata and srcMetadata
                  and tgtMetadata.ports[pname] and srcMetadata.ports[srcname]
                  and tgtMetadata.ports[pname].type==srcMetadata.ports[srcname].type 
                  and tgtMetadata.ports[pname].geom_sem and srcMetadata.ports[srcname].geom_sem
                  and tgtMetadata.ports[pname].geom_sem==srcMetadata.ports[srcname].geom_sem 
                  then
                    if srcTgtConn == true and srcType=='ORO' then 
                      rtt.logl("Error","Doubtfull connection possibilities between "..srcComp:getName().."."..pname.." and the ports of "..tgtComp:getName())
                    elseif srcTgtConn == true then
                      rtt.logl("Error","Doubtfull connection possibilities between "..src.package.."."..pname.." and the ports of "..tgtComp:getName())
                    elseif srcType=='ORO' then
                  rtt.logl("Info","Connecting "..srcComp:getName().."."..srcname.." and "..tgtComp:getName().."."..pname)
                  depl:connect(srcComp:getName().."."..srcname, tgtComp:getName().."."..pname, cp)
                  srcTgtConn = true
                    else
                  rtt.logl("Info","Connecting "..srcMetadata.ports[srcname].topic.."."..srcname.."and "..tgtComp:getName().."."..pname)
                  roscp.name_id = srcMetadata.ports[srcname].topic
                  depl:stream(tgtComp:getName().."."..pname, roscp)       
                  srcTgtConn = true
                    end
                  end
                end
              end
              if not srcTgtConn and srcType == 'ORO' then
                rtt.logl("Warning","Could not connect "..pname.." of (tgt) "..tgtComp:getName().." with a port of component (src) "..srcComp:getName()..": is this an error?")
              elseif not srcTgtConn then
                rtt.logl("Warning","Could not connect "..pname.." of "..tgtComp:getName().." with a topic of package "..src.package..": is this an error?")
              end
            end
          end
        -- >> ROS - ROS << --
        else
          rtt.logl("Error","UNDER CONSTRUCTION: situation that rewire between two ROS topics")
        end
      -- -- ALL - !ALL
      elseif tab.from == "all" then
        rtt.logl("Error","UNDER CONSTRUCTION: situation that rewire from=all, and to~=all")
      -- -- !ALL - ALL
      elseif tab.to == "all" then
        rtt.logl("Error","UNDER CONSTRUCTION: situation that rewire from~=all, and to=all")
      -- -- !ALL - !ALL
      else
        rtt.logl("Error","UNDER CONSTRUCTION: situation that rewire from~=all, and to~=all")
      end
    end
end

--- rewire
-- @param obj Rewire object
-- @param depl deployer
function do_rewire(obj, depl)
    connect(obj._srcref, obj._tgtref, obj, depl)
end

--- connect robot driver
-- @param obj object that has driver
-- @param depl deployer
function connect_robot_driver(obj, depl)
  connect(obj, obj._driverref, {{from="all",to="all"}}, depl)
end

--- connect external input
-- @param obj object that is an external input
-- @param depl deployer
function connect_external_input(obj, depl)
  local roscp = rtt.Variable('ConnPolicy')
  local srcComp, srcProps, srcMetadataProp, srcMetadataFun, srcMetadata
  local portname
  roscp.transport = 3 --ros
print(obj._parent._objectref._component_name)
  if obj._parent._objectref  then
    srcComp = obj._parent._objectref._component
  else
    srcComp = obj._parent._robotref._component
  end
  --load metadata
  srcProps = srcComp:getPropertyNames()
  for i,v in ipairs(srcProps) do
    if v=="metadata" then
      srcMetadataProp = srcComp:getProperty("metadata")
      srcMetadataFun = loadstring("return "..srcMetadataProp:get())
      if srcMetadataFun then
        srcMetadata=srcMetadataFun()
      else
        rtt.logl("Error","Syntax error in the metadata property of component "..srcComp:getName())
      end
      break
    end
  end
  --look for the port to connect to
  for i,v in pairs(srcMetadata.ports) do
    if v.type=='external' then
      if v.geom_sem=='PoseCoordinateSemantics(ee,ee,ee,b,b,b,b)' then
        portname = i
        break
      end
    end
  end
  roscp.name_id = obj.topic 
  depl:stream(srcComp:getName().."."..portname, roscp)
  rtt.logl("Info","Connected "..srcComp:getName().."."..portname.." to ROS topic "..obj.topic)
end

-----------------------------------
-- functions for itasc level setup
-----------------------------------
--- configure a robot
-- @param obj robot object
function config_robot_object(obj)
    if not obj._component:configure() then
      rtt.logl("Error","Configuring "..obj._component_name.." failed.")
    end
end    

--- add a robot to the Scene
-- @param sceneElement SceneElement object
-- @param scene scene where to add the robot
function add_robot(sceneElement, scene)
    local kdlFrame = rtt.Variable("KDL.Frame") --default is Unity rotation, zero position
    if instance_of(Frame, sceneElement.location) then
      if sceneElement.robot then
        kdlFrame:fromtab(sceneElement.location)
        if not scene:addRobot(sceneElement._robotref._component_name, kdlFrame) then
          rtt.logl("Error","Adding robot "..sceneElement._robotref._component_name.." failed.")
        end
      elseif sceneElement.object then
        kdlFrame:fromtab(sceneElement.location)
        if not scene:addRobot(sceneElement._objectref._component_name, kdlFrame) then
          rtt.logl("Error","Adding object "..sceneElement._objectref._component_name.." failed.")
        end
      end
    elseif instance_of(ExternalInput, sceneElement.location) then
      -- TODO for now if external input= wrt. to world, put T_b_w=Unity rotation, zero position
      if sceneElement.robot then
        if not scene:addRobot(sceneElement._robotref._component_name, kdlFrame) then
          rtt.logl("Error","Adding robot "..sceneElement._robotref._component_name.." failed.")
        end
      elseif sceneElement.object then
        if not scene:addRobot(sceneElement._objectref._component_name, kdlFrame) then
          rtt.logl("Error","Adding object "..sceneElement._objectref._component_name.." failed.")
        end
      end
    else
      rtt.logl("Error","unknown location type: unable to add robot")
    end
end

--- add objectframes
-- @param obj robot or object with object frames
-- @param scene
function add_objectframes(obj, scene)
    if obj._ObjectFrames then
      for FrameName, FullFrameName in pairs(obj._ObjectFrames) do
        if not scene:addObjectFrame(FullFrameName, FrameName, obj.name) then
          rtt.logl("Error","Adding objectframe " .. FullFrameName .. " failed")
        end
      end
    else
      rtt.logl("Warning","No object frames specified for "..obj.name)
    end
end

--- add vkc
-- @param obj vkc object
-- @param scene
function add_vkc(obj, scene)
    local _,_,_, objA=string.find(obj.o1, "(.+)%.(.+)")
    local _,_,_, objB=string.find(obj.o2, "(.+)%.(.+)")
	if not scene:addVirtualKinematicChain("VKC_"..obj._parent.name, obj._robotobj_o1_ref._ObjectFrames[objA], obj._robotobj_o2_ref._ObjectFrames[objB]) then
      rtt.logl("Error","Couldn't add virtualKinematicChain for task " .. obj._parent.name)
    end
end

--- add cc
-- @param obj cc object
-- @param scene
function add_cc(obj, scene)
    local vkcref
    local o1ref
    local o2ref
    local robotref
    if obj._parent.vkc then
      vkcref = "VKC_"..obj._parent.name
      robotref = ""
    else
      vkcref = ""
      robotref = obj._parent._robotref._component_name
    end
  	if not scene:addConstraintController("CC_"..obj._parent.name, robotref, vkcref , obj.prio_num) then
      rtt.logl("Error","Couldn't add constraint for task " .. obj._parent.name)
	end
end

--- add solver
-- @param obj solver object
-- @param scene scene
function add_solver(obj, scene)
   if not scene:addSolver(obj._component_name) then
     rtt.logl("Error","Couldn't add solver "..obj._component_name) 
   end
end

--- lua table to rtt string[]
-- @param tab table to convert
-- @return rtttab converted table
function tab2stringtab(tab)
      local output = rtt.Variable("string[]")
      output:resize(#tab)
      output:fromtab(tab)        
      return output
end

--- add fsm to event firer
--@param fsm fsm object to add port connections to
--@param comp event firer component
--@param level task/itasc/application
function add_fsm2event_firer(fsm, comp, level)
    local port
    local bcp = rtt.Variable('ConnPolicy')
    bcp.type=1 -- type buffered
    bcp.size=BUFFERED_EVENT_CONN_SIZE -- buffer size
    if level=='itasc' then
      --input to itasc
      port = rttlib.port_clone_conn(fsm._component:getPort("itasc_common_events_in"))
      comp:addPort(port, fsm._component_name.."_common_event", "common event to itasc level supervisor "..fsm._component_name)
      port = rttlib.port_clone_conn(fsm._component:getPort("itasc_priority_events_in"))
      comp:addPort(port, fsm._component_name.."_priority_event", "priority event to itasc level supervisor "..fsm._component_name)
      --output from itasc
      -- common
      port = port_clone_unconn(fsm._component:getPort("itasc_common_events_out"))
      comp:addPort(port, "itasc_common_events_out", "common events from itasc level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".itasc_common_events_out", comp:getName().."."..port:info().name, bcp)
      -- priority
      port = port_clone_unconn(fsm._component:getPort("itasc_priority_events_out"))
      comp:addPort(port, "itasc_priority_events_out", "priority events from itasc level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".itasc_priority_events_out", comp:getName().."."..port:info().name, bcp)
      --clone of input to itasc as output from itasc
      -- common
      port = port_clone_unconn(fsm._component:getPort("itasc_common_events_in_repeater"))
      comp:addPort(port, "itasc_common_events_in_repeater", "common events repeated from itasc level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".itasc_common_events_in_repeater", comp:getName().."."..port:info().name, bcp)
      -- priority
      port = port_clone_unconn(fsm._component:getPort("itasc_priority_events_in_repeater"))
      comp:addPort(port, "itasc_priority_events_in_repeater", "priority events repeated from itasc level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".itasc_priority_events_in_repeater", comp:getName().."."..port:info().name, bcp)
    elseif level=='application' then
      --input to itasc
      port = rttlib.port_clone_conn(fsm._component:getPort("application_common_events_in"))
      comp:addPort(port, fsm._component_name.."_common_event", "common event to application level supervisor "..fsm._component_name)
      port = rttlib.port_clone_conn(fsm._component:getPort("application_priority_events_in"))
      comp:addPort(port, fsm._component_name.."_priority_event", "priority event to application level supervisor "..fsm._component_name)
      --output from itasc
      -- common
      port = port_clone_unconn(fsm._component:getPort("application_common_events_out"))
      comp:addPort(port, "application_common_events_out", "common events from application level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".application_common_events_out", comp:getName().."."..port:info().name, bcp)
      -- priority
      port = port_clone_unconn(fsm._component:getPort("application_priority_events_out"))
      comp:addPort(port, "application_priority_events_out", "priority events from application level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".application_priority_events_out", comp:getName().."."..port:info().name, bcp)
      --clone of input to itasc as output from itasc
      -- common
      port = port_clone_unconn(fsm._component:getPort("application_common_events_in_repeater"))
      comp:addPort(port, "application_common_events_in_repeater", "common events repeated from application level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".application_common_events_in_repeater", comp:getName().."."..port:info().name, bcp)
      -- priority
      port = port_clone_unconn(fsm._component:getPort("application_priority_events_in_repeater"))
      comp:addPort(port, "application_priority_events_in_repeater", "priority events repeated from application level supervisor "..fsm._component_name)
      depl:connect(fsm._component_name..".application_priority_events_in_repeater", comp:getName().."."..port:info().name, bcp)
    elseif level=='task' then
      rtt.logl("Error","TODO: Event firer to task level FSMs not implemented yet")
    else
      rtt.logl("Error","Unable to fire an event to a "..level.." level FSM")
    end
end

--- create topics for the event firer
-- @param event_firer name of event firer component
-- @param pname name of port to connect to topic
-- @param topic name of topic
function create_event_firer_topics(event_firer, pname, topic)
    local roscp = rtt.Variable('ConnPolicy')
    roscp.transport = 3 --ros
    local roscpb = rtt.Variable('ConnPolicy')
    roscpb.transport = 3 --ros
    roscpb.type = 1 --buffered
    roscpb.size = BUFFERED_EVENT_CONN_SIZE --size of buffer
    local portnames = event_firer:getPortNames()
    
    for i,v in ipairs(portnames) do
      if event_firer:getPort(v) and event_firer:getPort(v):info().porttype=='in' and event_firer:getPort(v):info().type=="/std_msgs/String" then
        roscp.name_id = "/itasc/"..event_firer:getPort(v):info().name
        depl:stream(event_firer:getName().."."..event_firer:getPort(v):info().name, roscp)
      elseif event_firer:getPort(v) and event_firer:getPort(v):info().porttype=='out' and event_firer:getPort(v):info().type=="/std_msgs/String" then
        roscpb.name_id = "/itasc/"..event_firer:getPort(v):info().name
        depl:stream(event_firer:getName().."."..event_firer:getPort(v):info().name, roscpb)
      end
    end
end
  
-- helper functions
-------------------
-- Application level checking.
function foreach(f, tab)
   if not tab then return end 
   for i,v in pairs(tab) do f(v,i) end 
end

--- Copied  from utils.
--- Pre-order tree traversal.
-- @param fun function to apply to each node
-- @param root root to start from
-- @param pred predicate that nodes must be satisfied for function application.
-- @return table of return values
function maptree(fun, root, pred)
   local res = {}
   local function __maptree(tab)
      --v = a value of the table, k = index/identifier of that value, tab = table to     apply maptree on = table that contains k,v pairs
      foreach(function(v, k)
         if not is_meta(k) then
            if not pred or pred(v) then res[#res+1] = fun(v, tab, k) end 
            if type(v) == 'table' then __maptree(v) end 
         end 
          end, tab)
   end 
   __maptree(root)
   return res 
end

--- actual deploy function
-- @param model model to deploy
function deploy(model, ReportProp)
  -- add here the function calls to the module with the deploy functions
  -- model contains the table with all info!
  rtt.logl("Info","execute code")

  tc = rtt.getTC()
  -- started using lua-browser
  if tc:getName() == "lua" then
    depl=tc:getPeer("Deployer")
  --started using std orocos task-browser 
  elseif tc:getName() == "Deployer" then
    depl=tc
  end
  
  -- import packages
  rtt.logl("Info","--importing packages--")
  -- -- importing default itasc packages
  depl:import("ocl")
  depl:import("kdl_typekit")
  depl:import("rtt_rosnode")
  depl:import("itasc_core")
  --depl:import("rtt_geometric_semantics_tf_msgs") --used for the externalInput
  
  -- -- importing model dependent packages
  maptree(function (v) 
        import_package(v, depl)
      end, model, is_package)
  
  -- importing metadata from rospackages (needs driver to be loaded in order to find evt. supervisor)
  maptree(function (v)
        import_metadata(v, depl)
      end, model, is_package) 
  
  -- load components
  rtt.logl("Info","--loading components--")
  -- -- loading default components
  local TimerName = "Timer"
  local SceneName = "Scene"
  local EventFirerName = "event_firer"
  local ReporterName = "Reporter"
  depl:loadComponent(TimerName,"OCL::TimerComponent")
  depl:loadComponent(SceneName,"iTaSC::Scene")
  depl:loadComponent(EventFirerName, "OCL::LuaComponent")
  if ReportProp then
      depl:loadComponent(ReporterName, "OCL::FileReporting")
  end
  
  timer = depl:getPeer(TimerName)
  scene = depl:getPeer(SceneName)
  event_firer = depl:getPeer(EventFirerName)
  if ReportProp then
    reporter = depl:getPeer(ReporterName)
  end
  event_firer:exec_file(event_firer_file)
  event_firer:configure()

  -- -- loading model dependent components
  maptree(function (v)
        v._component_name = load_component(v,depl)
        v._component = depl:getPeer(v._component_name)
      end, model, is_component)
  
  -- -- creating an empty lua component for the supervisors
  maptree(function (v)
        v._component_name = create_lua_component(v, depl)
        v._component = depl:getPeer(v._component_name)
      end, model, is_FSM)

  -- set activities
  rtt.logl("Info","--setting activities--")
  -- -- params: period, priority, schedtype=ORO_SCHED_OTHER or ORO_SCHED_RT
  depl:setActivity(TimerName, 0, 0, rtt.globals.ORO_SCHED_RT)
  depl:setActivity(SceneName, 0, 0, rtt.globals.ORO_SCHED_RT)
  depl:setActivity(EventFirerName, EVENT_FIRER_PERIOD, 0, rtt.globals.ORO_SCHED_OTHER)
  -- -- give deployer activity to run evt. scripts loaded in it
  depl:setActivity("Deployer", 1, 0, rtt.globals.ORO_SCHED_OTHER)
  if ReportProp then
      depl:setActivity(ReporterName, REPORTER_PERIOD, 1, rtt.globals.ORO_SCHED_OTHER)
  end 
  
  -- -- setting acitivities of model dependent components
  maptree(function (v)
        local period
        if is_itascORtasklevel_component(v) or is_FSM(v) then
          period = 0.0
        else
          period = application_period
        end
        set_activity(v,depl, period, priority, schedtype)
      end, model, is_component_or_FSM)
  
  -- connect peers
  rtt.logl("Info","--connecting peers--")
  -- -- default peer connections
  if ReportProp then
  -- -- all components are reporter peers
  maptree(function (v)
          connect_peers(v, depl, ReporterName)
      end, model, is_component_or_FSM)
  end
  
  -- -- model dependent peer connections
  -- -- -- make all itasc level components peers of Scene
  maptree(function (v)
          add_peers(v, depl, SceneName)
      end, model, is_itascORtasklevel_component)
  maptree(function (v)
          v._component:addPeer(scene)
      end, model, is_FSM)
  
  -- -- -- connect peers of application level supervisor
  maptree(function (v)
          add_peers(v, depl, model.fsm._component_name)
      end, model, is_applicationlevel_component)
  
  -- -- -- connect peers of itasc level supervisor
  if model.itasc.fsm then
      maptree(function (v)
              add_peers(v, depl, model.itasc.fsm._component_name)
          end, model, is_itasclevel_component)
      maptree(function (v)
              if umf.uoo_type(v._srcref) == 'instance' and instance_of(SetpointGenerator, v._srcref) then
                add_peers(v._srcref, depl, model.itasc.fsm._component_name)
              end
              if umf.uoo_type(v._tgtref) == 'instance' and instance_of(SetpointGenerator, v._tgtref) then
                add_peers(v._tgtref, depl, model.itasc.fsm._component_name)
              end
          end, model, is_Rewire)
  
  end
  
  -- -- -- connect peers of task level supervisor
  maptree(function (v)
        add_peers(v, depl, v._parent.fsm._component_name)
    end, model, is_tasklevel_component)
  
  -- -- -- connect components that are referenced by a Rewire to the corresponding supervisor
  maptree(function (v)
          if v._tgtref then
            add_peers(v._tgtref, depl, v._parent._parent.fsm._component_name)
          end
          if v._srcref then
            add_peers(v._srcref, depl, v._parent._parent.fsm._component_name)
          end
      end, model, is_Rewire)
  
  rtt.logl("Info","--loading and configuring skills--")
  -- load skills in supervisors
  maptree(load_skill, model, is_FSM)
  
  -- configure supervisors
  maptree(configure_skill, model, is_FSM)
  
  -- deploy driver scripts 
  rtt.logl("Info","Deploy driver scripts in the same process, expects script to return before continuing deployment")
  local driver_file
  local driver_setup = false
  local driver_conf={}
  maptree(function (v)
      if v.file then
        rtt.logl("Info","loading driver script: ".. filespec2file(v.file))
        driver_file = dofile(filespec2file(v.file))
        driver_conf.timer = TimerName
        driver_conf.event_firer = EventFirerName
        driver_conf.reporter = ReporterName 
        driver_conf.prefix = v.name
        if v.config then
          for i, val in ipairs(v.config) do
            if type(val)=='string' then
              if driver_conf.config then
                rtt.logl("Error","UNDER CONSTRUCTION: At the moment only one configuration file allowed for a driver: "..v.name)
              end
              driver_conf.config = filespec2file(val)
            end
          end
        end
        maptree(function (vaf)
            if driver_conf.application_supervisor then
              rtt.logl("Error","Multiple application level supervisors found, expected one (will use last found)")
            end
            driver_conf.application_supervisor = vaf._component_name 
          end, model, is_applicationlevel_FSM)
        driver_setup = driver_file.deploy(driver_conf) 
        if not driver_setup then
          rtt.logl("Error","Unable to execute "..tostring(filespec2file(v.file)))
        else
          rtt.logl("Info","Successfully executed driver "..tostring(filespec2file(v.file)))
        end
        --process setup info from the driver
        v._supervisor_name = driver_setup.supervisor
        v._supervisor = depl:getPeer(v._supervisor_name)
        v._setup = driver_setup
      end
    end, model, is_Driver)
  
  -- deploy setpoint generator scripts 
  rtt.logl("Info","Deploy setpoint generator scripts in the same process, expects script to return before continuing deployment")
  local setpgen_file
  local setpgen_setup = false
  local setpgen_conf={}
  maptree(function (v)
      if v.file then
        rtt.logl("Info","loading setpoint generator script: ".. filespec2file(v.file))
print(filespec2file(v.file))
        setpgen_file = dofile(filespec2file(v.file))
        setpgen_conf.timer = TimerName
        setpgen_conf.event_firer = EventFirerName
        setpgen_conf.reporter = ReporterName 
        setpgen_conf.prefix = v.name
        if v.config then
          for i, val in ipairs(v.config) do
            if type(val)=='string' then
              if setpgen_conf.config then
                rtt.logl("Error","UNDER CONSTRUCTION: At the moment only one configuration file allowed for a setpoint generator: "..v.name)
              end
              setpgen_conf.config = filespec2file(val)
            end
          end
        end
        maptree(function (vaf)
            if setpgen_conf.application_supervisor then
              rtt.logl("Error","Multiple application level supervisors found, expected one (will use last found)")
            end
            setpgen_conf.application_supervisor = vaf._component_name 
          end, model, is_applicationlevel_FSM)
        -- the actual execution of the script
        setpgen_setup = setpgen_file.deploy(setpgen_conf) 
        if not setpgen_setup then
          rtt.logl("Error","Unable to execute setpoint generator script "..tostring(filespec2file(v.file)))
        else
          rtt.logl("Info","Successfully executed setpoint generator script "..tostring(filespec2file(v.file)))
        end
        --process setup info from the setpgen
        v._supervisor_name = setpgen_setup.supervisor
        v._supervisor = depl:getPeer(v._supervisor_name)
        v._setup = setpgen_setup
      end
    end, model, is_SetpointGenerator)
  
  rtt.logl("Info","--loading properties--")
  -- load property files
  if ReportProp then
      depl:loadService(ReporterName, "marshalling")
  end
  
  maptree(function (obj) 
          load_marshalling(obj, depl)
      end, model, is_component_or_FSM)
  
  maptree(update_properties, model, has_config)
  
  -- load reporter properties
  if ReportProp then
      if not type(ReportProp)=='string' then
        rtt.logl("Error","ReportProp is not a string but a "..type(ReportProp))
      else
        local filename = filespec2file(ReportProp)
        local repup = reporter:provides("marshalling"):getOperation("loadProperties")
        repup(filename)
      end
  end
  
  -- connect ports
  rtt.logl("Info","--connecting ports--")
  connect_timer(model.fsm, depl, TimerName)
  connect_timer(model.itasc.fsm, depl, TimerName)
  connect_itasclevel_supervisor(model.itasc.fsm, depl, model.fsm)
  
  maptree(function (obj)
          connect_tasklevel_supervisor(obj, depl, model.itasc.fsm, model.fsm)
      end, model, is_tasklevel_FSM)
  
  maptree(function (obj)
          event_connect(obj, depl, model.fsm)
      end, model, is_applicationlevel_component_or_supervisor)

  maptree(function (obj)
          event_connect(obj, depl, model.itasc.fsm)
      end, model, is_itascORtasklevel_component)
  
  maptree(function (obj)
          connect_referenced_eventports(obj, depl, obj._parent._parent.fsm)
          connect_referenced_eventports(obj, depl, model.itasc.fsm)
      end, model, is_Rewire)
  
  maptree(function (obj) connect_cc_vkc(obj, depl) end, model, is_Task)
  
  maptree(function (obj) do_rewire(obj, depl) end, model, is_Rewire)
  
  maptree(function (obj) connect_robot_driver(obj, depl) end, model, has_driver)

  maptree(function (obj) connect_external_input(obj, depl) end, model, is_ExternalInput)
  
  create_event_firer_topics(event_firer)
  
  rtt.logl("Info","--adding event firer--")
  maptree(function (obj) add_fsm2event_firer(obj, event_firer, 'itasc') end, model, is_itasclevel_FSM) 
  maptree(function (obj) add_fsm2event_firer(obj, event_firer, 'application') end, model, is_applicationlevel_FSM) 
  
  rtt.logl("Info","--setting up timer--")
  -- configure and start timer
  timer:configure()
  timer:start()
  if ReportProp then
      reporter:configure()
  end
  
  -- create component tables
  local setpointGeneratorTable={}
  local driverTable={}
  local robotTable={}
  local taskTable={}
  local solverTable={}
  local compositeTable={} -- table of supervisors at the composite level
  maptree(function (obj)
      setpointGeneratorTable[#setpointGeneratorTable+1] = obj._component_name
      end, model, is_SetpointGenerator) 
  maptree(function (obj)
      driverTable[#driverTable+1] = obj._component_name
      end, model, is_Driver) 
  maptree(function (obj)
      robotTable[#robotTable+1] = obj._component_name
      end, model, is_Robot_Object) 
  maptree(function (obj)
      taskTable[#taskTable+1] = obj.name
      end, model, is_Task)
  maptree(function (obj)
      solverTable[#solverTable+1] = obj._component_name
      end, model, is_Solver)
  maptree(function (obj)
      compositeTable[#compositeTable+1] = obj._supervisor_name
      end, model, has_compositelevel_supervisor)
  -- add ITASC as default in the composite table
  compositeTable[#compositeTable+1] = 'ITASC'
  
  -- -- convert tables to rtt variables
  rttRobotTable = tab2stringtab(robotTable)
  rttSetpointGeneratorTable = tab2stringtab(setpointGeneratorTable)
  rttDriverTable = tab2stringtab(driverTable)
  rttTaskTable  = tab2stringtab(taskTable)
  rttSolverTable= tab2stringtab(solverTable)
  rttCompositeTable= tab2stringtab(compositeTable)

  -- -- set these tables in the application level supervisors
  maptree(function (v)
        -- first check if the properties exist
        local proptab = v._component:getPropertyNames()
        local SPTflag, driverTflag, compTflag = false, false, false
        for i,k in ipairs(proptab) do
          if k=='setpointGeneratorTable' then  SPTflag=true end
          if k=='driverTable' then  driverTflag=true end
          if k=='compositeTable' then compTflag=true end
        end 
        if SPTflag and driverTflag and compTflag then
          v._component:getProperty("setpointGeneratorTable"):set(rttSetpointGeneratorTable)
          v._component:getProperty("driverTable"):set(rttDriverTable)
          v._component:getProperty("compositeTable"):set(rttCompositeTable)
        else
          rtt.logl("Error","Component "..v._component_name.." does not have the required properties: setpointGeneratorTable, driverTable and compositeTable")
        end
      end, model, is_applicationlevel_FSM)
  
  -- -- set these tables in the itasc level supervisors
  maptree(function (v)
        -- first check if the properties exist
        local proptab = v._component:getPropertyNames()
        local robotTflag, taskTflag, solverTflag, sceneTflag, reportTflag = false, false, false, false, false
        for i,k in ipairs(proptab) do
          if k=='robotTable' then  robotTflag=true end
          if k=='taskTable' then  taskTflag=true end
          if k=='solverTable' then  solverTflag=true end
          if k=='scene' then  sceneTflag=true end
          if k=='reporter' then reportTflag=true end
        end 
        if robotTflag and taskTflag and solverTflag and sceneTflag and reportTflag then
          v._component:getProperty("robotTable"):set(rttRobotTable)
          v._component:getProperty("taskTable"):set(rttTaskTable)
          v._component:getProperty("solverTable"):set(rttSolverTable)
          v._component:getProperty("scene"):set(SceneName)
          v._component:getProperty("reporter"):set(ReporterName)
        else
          rtt.logl("Error","Component "..v._component_name.." does not have the required properties: robotTable, taskTable, solverTable and scene")
        end
      end, model, is_itasclevel_FSM)
  
  rtt.logl("Info","--itasc level setup--")
  -- itasc level setup (former itasc_configuration.lua file)
  maptree(config_robot_object, model, is_Robot_Object)
  maptree(function (obj)
          add_robot(obj, scene)
      end, model, is_SceneElement)
  maptree(function (obj)
          add_objectframes(obj, scene)
      end, model, is_Robot_Object)
  maptree(function (obj)
          add_vkc(obj, scene)
      end, model, is_VKC)
  maptree(function (obj)
          add_cc(obj, scene)
      end, model, is_CC)
  maptree(function (obj)
          add_solver(obj, scene)
      end, model, is_Solver)
  
  rtt.logl("Info","--starting supervisors--")
  -- start supervisors (order of importance!)
  -- 1) start task level supervisors
  maptree(function (v)
          v._component:start()
      end, model, is_tasklevel_FSM)
  -- 2) start itasc level supervisors
  maptree(function (v)
          v._component:start()
      end, model, is_itasclevel_FSM)
  -- 3) start application level supervisor
  model.fsm._component:start()
  
  rtt.logl("Info","--starting the timer--")
  -- setup timer
  timer:startTimer(model.fsm._component:getProperty("application_timer_id"):get(), 0.01)
  event_firer:start()
  
end
